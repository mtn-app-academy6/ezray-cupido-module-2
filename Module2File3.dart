void main() {
  var Contestant = new MTNAppAwardsWinner();
  Contestant.Name = "Ambani Africa";
  Contestant.Category = "Education";
  Contestant.Developer = "Mukundi Lambani";
  Contestant.Year = 2021;
  Contestant.PrintWinnerInformation();
}

class MTNAppAwardsWinner {
  String? Name;
  String? Category;
  String? Developer;
  int? Year;

  void PrintWinnerInformation() {
    print("MTN App Awards Winner Details:");
    print(Name?.toUpperCase());
    print("The App Category is $Category");
    print("The Developer name is $Developer");
    print("The Winning Year is $Year");
  }
}
